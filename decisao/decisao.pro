TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11

SOURCES += decisao.cpp

HEADERS += \
    view.h \
    node.h

LIBS += -L/usr/lib -lsfml-graphics -lsfml-window -lsfml-system
