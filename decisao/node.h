#ifndef NODE_H
#define NODE_H
#include <list>
using namespace std;

//struct Node;
//using u_node = shared_ptr<Node>;
//using u_node = Node *;

struct VisualNode{
    bool foco{false};
};

struct Node : public VisualNode{
    string escolha;
    string texto;
    list<shared_ptr<Node> > filhos;
    Node(string e = " vazia", string t = " vazia"):
        escolha(e), texto(t)
    {}

    string str(){
        stringstream ss;
        ss << escolha << endl << texto;
        return ss.str();
    }

    void show(){
        cout << "Escolha:" << escolha << endl;
        cout << "Texto:" << texto << endl;
        for(auto &x : filhos)
            cout << x->escolha << endl;
    }
};
#endif // NODE_H

