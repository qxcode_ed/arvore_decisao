#ifndef VIEW_H
#define VIEW_H


#include <iostream>
#include <string>
#include <cmath>
#include <list>

#include <SFML/Graphics.hpp>

#include <thread>
#include <memory> //unique_ptr
#include <mutex>


using namespace std;
using namespace sf;
class StateView{

public:

    StateView(RenderWindow * janela)
    {
        _janela = janela;
        if(!font.loadFromFile(fontPath))
            cerr << "Fonte " << fontPath << " nao encontrada." << endl;
        text.setFont(font);
        text.setCharacterSize(14);
    }

    void init(){
        _thread = move(unique_ptr<thread>(new thread(&StateView::run, this)));
    }

    void close(){
        _janela->close();
        _thread->join();
    }

protected:
    sf::RenderWindow * _janela;
    unique_ptr<thread> _thread;
    sf::Font font;
    sf::Text text;
    void drawSimpleLine(Vector2f a, Vector2f b, Color color = sf::Color::White){
        sf::Vertex line[2] =
        {
            sf::Vertex(a, color),
            sf::Vertex(b, color)
        };
        _janela->draw(line , 2, sf::Lines);
    }

    void drawText(Vector2f pos, string texto, Color color = sf::Color::White){
        this->text.setColor(color);
        this->text.setPosition(pos);
        this->text.setString(texto);
        _janela->draw(this->text);
    }

    virtual void view() = 0;

private:

    const string fontPath = "/usr/share/fonts/truetype/droid/DroidSansMono.ttf";
    std::function<void()> _fn;

    void run(){
        while(_janela->isOpen()){
            sf::Event event;
            while(_janela->pollEvent(event)){
                if(event.type == sf::Event::Closed){
                    _janela->close();
                }
                else if (event.type == sf::Event::Resized){
                    _janela->setView(sf::View(
                             sf::FloatRect(0, 0, event.size.width, event.size.height)));
                }
            }
            view();//chama a funcao de desenho
        }
    }
};

class TreeViewH : public StateView{
public:
    TreeViewH(RenderWindow * janela, const shared_ptr<Node> &node):
        StateView(janela),
        _root(node)
    {
        init();
    }
    virtual void view(){
        auto tam = _janela->getSize();
        _janela->clear();
        drawOne(_root, Vector2f(0, tam.y/2));
        drawKids(_root, Vector2f(0, tam.y/2), tam.y);
        _janela->display();
    }

    void drawOne(const shared_ptr<Node> &node, Vector2f pos){
        text.setString(node->texto);
        text.setPosition(pos + Vector2f(10, 0));
        Color color = node->filhos.size() != 0 ? Color::Cyan : Color::Green;
        if(node->foco)
            color = Color::Red;
        text.setColor(color);
        _janela->draw(text);
    }

    void drawKids(const shared_ptr<Node> &node, Vector2f pos, float espaco){
        int tam = node->filhos.size();
        float espacoKid = espaco / tam;

        //for(int i = 0; i < tam; i++){
        int i = 0;
        for(const auto &x : node->filhos){
            auto pKid = Vector2f(pos.x + 150, pos.y - espaco/2 + espacoKid * (i + 0.5));
            drawSimpleLine(pos, pKid);
            drawText((pos + pKid)/2.f, x->escolha, Color::Yellow);
            drawOne(x, pKid);
            drawKids(x, pKid, espacoKid);
            i++;
        }

    }

private:
    shared_ptr<Node> _root;
};

#endif // VIEW_H

