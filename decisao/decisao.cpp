#include <iostream>
#include <vector>
#include <memory>
#include <sstream>
#include <fstream>
#include "node.h"
#include "view.h"

using namespace std;

const string STR_BACK = ";";
const char STR_SEP = '#';

void inline cin_clear(){
	cin.ignore(1000, '\n');
}

inline std::string trim(std::string& str)
{
    str.erase(0, str.find_first_not_of(' '));       //prefixing spaces
    str.erase(str.find_last_not_of(' ')+1);         //surfixing spaces
    return str;
}



shared_ptr<Node> root = nullptr;

void tree_show(const shared_ptr<Node> &node, int nivel){
    if(node == nullptr)
        return;
    if(node->foco)
        cout << "->";
    else
        cout << "  ";
    cout << string(nivel, ' ') << node->escolha << " ; ";
    cout << node->texto << endl;
    for(auto &x : node->filhos)
        tree_show(x, nivel + 4);
}


void tree_save(ofstream &of, int level, const shared_ptr<Node> &node){
    of << string(level, ' ') << node->escolha
       << STR_SEP << node->texto << "\n";
    for(auto &x : node->filhos)
        tree_save(of, level + 2, x);
    of << string(level, ' ') << STR_BACK << endl;
}

void tree_load(ifstream &ifs, shared_ptr<Node> &node){
    string str;
    while(true){
        getline(ifs, str);
        trim(str);
        if(str == STR_BACK)
            return;
        stringstream ss(str);
        shared_ptr<Node> kid(new Node);
        getline(ss, kid->escolha, STR_SEP);
        getline(ss, kid->texto, '\n');
        if(node == nullptr){
            node = move(kid);
            continue;
        }else{
            node->filhos.push_back(move(kid));
            tree_load(ifs, node->filhos.back());
        }
    }
}

shared_ptr<Node> match_resp(const shared_ptr<Node> & node, string ans){
    for(const auto &x : node->filhos){
        if(x->escolha.find(ans) == 0)
            return x;
    }
    return nullptr;
}

void tree_make(const shared_ptr<Node> &node){
	const string options = string("\nSHELL\n") + 
       "[                              Back | b           ]\n" +
       "[                       Show arvore | s           ]\n" +
       "[               add escolha ESCOLHA | ae ESCOLHA  ]\n" +
       "[     add pergunta antes de ESCOLHA | ap ESCOLHA  ]\n" +
       "[                        rm ESCOLHA | rm ESCOLHA  ]\n" +
       "[              Ir para ESCOLHA      | go ESCOLHA  ]\n" +
       "[    Muda escolha do no pra ESCOLHA | ce ESCOLHA  ]\n" +
       "[        Muda texto do no pra TEXTO | ct TEXTO    ]\n" +
       ">> ";

	while(true){
		cout << endl;
        node->foco = true;
        tree_show(root, 0);
		cout << options;;
		string escolha;
		cin >> escolha;
        if(escolha == "ae"){
            shared_ptr<Node> no(new Node);
			getline(cin, no->escolha);
            trim(no->escolha);
			node->filhos.push_back(move(no));
        }if(escolha == "ap"){
            string escolha;
            cin >> escolha;
            cin_clear();
            auto filho = match_resp(node, escolha);
            if(filho != nullptr){
                shared_ptr<Node> no(new Node);
                cout << "Qual a nova pergunta a inserir? << endl";
                getline(cin, no->texto);
                trim(no->texto);
                no->escolha = filho->escolha;
                no->filhos.push_back(filho);
                node->filhos.push_back(move(no));
                node->filhos.remove(filho);
            }
        }else if(escolha == "ce"){
            getline(cin, node->escolha);
            trim(node->escolha);
        }else if(escolha == "ct"){
            getline(cin, node->texto);
            trim(node->texto);
        }else if(escolha == "s"){
            cin_clear(); tree_show(root, 0);
        }else if(escolha == "go"){
            string escolha;
            cin >> escolha;
            cin_clear();
            auto filho = match_resp(node, escolha);
            if(filho != nullptr){
                node->foco = false;
                tree_make(filho);
            }
        }else if(escolha == "rm"){
            string escolha;
            cin >> escolha;
            cin_clear();
            auto filho = match_resp(node, escolha);
            if(filho != nullptr){
                node->filhos.remove(filho);
            }
        }else if(escolha == "b"){
            node->foco = false;
			cin_clear(); return;
		}else{
			cin_clear(); cout << "Comando nao reconhecido" << endl;
		}
	}
	 
}

void tree_jogar(const shared_ptr<Node> &node){
    if(node->filhos.size() != 0){
        node->foco = true;
        shared_ptr<Node> filho;
        do{
            cout << "Pergunta: " << node->texto << endl;
            for(const auto &x : node->filhos)
                cout << x->escolha << endl;
            cout << ">> ";
            string escolha;
            getline(cin, escolha);
            filho = match_resp(node, escolha);
        }while(filho != nullptr);
        node->foco = false;
        tree_jogar(filho);
    }else{
        node->foco = true;
        cout << node->texto << endl;
        cout << "Game over. Digite [back] >> ";
        string str;
        while(str!= "back"){
            cin >> str;
        }
        cin_clear();
        node->foco = false;
    }
}

int main(){
const string path = "../decisao/tree.txt";
#if 0
    root = shared_ptr<Node>(new Node("root", "maxo?"));
    shared_ptr<Node> kid1(new Node("sim", "quantos filhos?"));
    root->filhos.push_back(kid1);
    kid1->filhos.push_back(shared_ptr<Node>(new Node("0", "Faca um!")));
    kid1->filhos.push_back(shared_ptr<Node>(new Node("1", "legal!")));
    kid1->filhos.push_back(shared_ptr<Node>(new Node("3", "massa!")));

    shared_ptr<Node> kid2(new Node("nao", "eh mulher?"));
    root->filhos.push_back(kid2);

    auto kid3 = shared_ptr<Node>(new Node("nao", "eh viado?"));
    kid2->filhos.push_back(kid3);
    kid2->filhos.push_back(shared_ptr<Node>(new Node("sim", "opa!")));

    kid3->filhos.push_back(shared_ptr<Node>(new Node("sim", "egua!")));
    kid3->filhos.push_back(shared_ptr<Node>(new Node("nao", "ein?!")));

#else
    ifstream ifs(path.c_str());
    if(ifs){
        tree_load(ifs, root);
        ifs.close();
    }else{
        cout << "Arquivo nao existe" << endl;
    }
#endif
    //RenderWindow janela(VideoMode(800, 600), "Janelinha");
    //TreeViewH tview(&janela, root);

    string op = "";
    while(op != "b"){
        cout << "op: [j|jogar], [a|alterar], [b|back], [s|show] >> ";
        cin >> op;
        cin_clear();
        if(op == "a")
            tree_make(root);
        else if(op == "b")
            continue;
        else if(op == "j")
            tree_jogar(root);
        else if(op == "s")
            tree_show(root, 0);
    }

    ofstream ofs(path.c_str());
    tree_save(ofs, 0, root);
    ofs.close();
    //tview.close();
    return 0;
}
